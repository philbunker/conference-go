from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_location_image(city, state):
    url = f'https://api.pexels.com/v1/search?query={city}, {state}'
    headers = {'Authorization': PEXELS_API_KEY}
    r = requests.get(url, headers=headers)
    response = json.loads(r.text)
    if response["total_results"] > 0:
        return response["photos"][0]["url"]
    return None

def get_conference_weather(location):
    # Fetch `lat` and `lon` from geocode API
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={location.city},{location.state},US&appid={OPEN_WEATHER_API_KEY}"
    r = requests.get(url)
    response = json.loads(r.text)
    if not response:
        return 'null'
    lat, lon = response[0]["lat"], response[0]["lon"]

    # Fetch weather data using `lat` and `lon`
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    r = requests.get(url)
    response = json.loads(r.text)

    return {'temp': response['main']['temp'], 'description': response['weather'][0]['description']}
